﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using UWSAT.Services;

namespace UWSAT.Controllers
{
    public class ModController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Latitute = Request.QueryString["la"];
            ViewBag.Longitude = Request.QueryString["lo"];
            ViewBag.Band = this.RouteData.Values["band"];
            if (this.RouteData.Values["tech"] != null)
                ViewBag.Tech = this.RouteData.Values["tech"];
            else
                ViewBag.Tech = "NA";
            ViewBag.APIRootURL = ConfigurationManager.AppSettings["APIRootURL"];
            return View();
        }

        [HttpPost]
        public ActionResult SendNotification(string numberOfConUsers, string dataRates,
            string location)
        {            
            string mailTo = ConfigurationManager.AppSettings["MailTo"];
            string subject = ConfigurationManager.AppSettings["EmailSubject"];
            string body = ConfigurationManager.AppSettings["EmailBoday"];
            body = string.Format(body, numberOfConUsers, dataRates, location);
            EmailService service = new EmailService();
            bool result = service.SendEmail(mailTo, subject, body);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}