var kmlreffn = '61E-Ku-EastH.kml';

var kmlkd =   'KK20-HH_Downlink_MRS1KHTRC10934500.kml';
var kmlgen2 = 'KK20-VV_Downlink_MRS1KVTRC10934500.kml';
var kmlku =   'KK20-HH_Uplink_MRS1KHREC12984500.kml';
var kmlgen1 = 'KK20-VV_Uplink_MRS1KVREC12984500.kml';
var kk9vvu = 'KK09-VV_Uplink_MRS1KVREC13149500.kml';
var kk9vvd = 'KK09-VV_Downlink_MRS1KVTRC11349500.kml';

var kmlgkey = 'AIzaSyB47HO4cv1NU4iqfo3LTKEHM1O9LZAHPpM';
var kmldirectdir = '/Data/';
var kmllhdir = 'https://localhost:44335/Data/';
var kmlpdir = 'https://ebq20api.azurewebsites.net/EBusiness/Data/'; 
var kmldir = kmldirectdir;


function onClick3(e) {
    var lat = e.latLng.lat();
    var long = e.latLng.lng();
    var host = window.location.origin;
    var band = "ku"; //path.substr(6);
    activeButton = document.activeButton;
    if (activeButton)
        band = activeButton.substring(0, 2);
    if ('c' == band[0]) //cBand
        band = "c";
    var arg = host + "/mod/" + band + "?la=" + lat + "&lo=" + long;
        var $dialog = $('<div style="padding:0;"></div>')
            .html('<iframe style="border: 0px;padding:0;margin:0; " src="' + arg + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: true,
                modal: true,
                height: 600,
                width: 850,
                padding: 0,
                draggable: true,
                title: "Quote",
                zIndex: 10000,
                movetotop: true,
                moveToTop: true
            }).dialog('open').dialog('moveToTop');
        alert("onClick3b done");
};

function contact(e) {
    console.log(e.toElement.value);
    if (e.toElement.value == 'Contact') {
        var lat = '15.487242337376845';//e.latLng.lat();
        var long = '30.16311329999998';//e.latLng.lng();
        var host = window.location.origin;
        var band = "ku"; //path.substr(6);
                
        var arg = host + "/contact/" + band + "?la=" + lat + "&lo=" + long;
        var $dialog = $('<div style="padding:0;"></div>')
            .html('<iframe style="border: 0px;padding:0;margin:0; " src="' + arg + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: true,
                modal: true,
                height: 600,
                width: 850,
                padding: 0,
                draggable: true,
                title: "Quote",
                zIndex: 10000,
                movetotop: true,
                moveToTop: true
            }).dialog('open').dialog('moveToTop');
        alert("onClick3b done");
    }
};

function onClick4(e) {
    if (e.LatLng != null) {
        //alert(e.LatLng);
        return;
    }
    if (e.name != null) {
        //alert(e.name);
    }
    contact(e);
    //alert(e);

}
    //var script = document.createElement("SCRIPT");
    //script//
    //script.type = 'text/javascript';
    //document.getElementsByTagName("head")[0].appendChild(script);
 


function initClickHandler(map) {
    google.maps.event.addListener(map, "click", function (e) {
        onClick3(e);
    });
    google.maps.event.addDomListener(window, "click", function (e) {
        onClick4(e);
    });
}


function initQuote(map) {
    //window.alert("InitQuote");
    var markers = [];
    initClickHandler(map);
    var service = new google.maps.places.PlacesService(map);
    var sb = document.getElementById('map_input');
    var mc = document.getElementById('map_contact');
    map.controls[google.maps.ControlPosition.TOP].push(sb);
    map.controls[google.maps.ControlPosition.TOP].push(mc);
    var searchBox = new google.maps.places.SearchBox(sb);
    //var button = new google.maps.places.alert(mc);

    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });
    var options = {
        bounds: map.getBounds(),
        types: ['(cities)']
    };
    autocomplete = new google.maps.places.Autocomplete(sb, options);
    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        if (places.length == 1) {
            var place = places[0];
            map.setCenter(place.geometry);
            //map.setCenter(place.geometry.location);
            return;
        }
        for (var i = 0, marker; marker = markers[i]; i++) {
            marker.setMap(null);
        }

        // For each place, get the icon, place name, and location.
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            var image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            var marker = new google.maps.Marker({
                map: map,
                icon: image,
                title: place.name,
                position: place.geometry.location
            });

            markers.push(marker);

            bounds.extend(place.geometry.location);
        }

        map.fitBounds(bounds);
    });


}



function initMapQuoteGM() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(31.6, 26.79),
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    initClickHandler(map);



};
