(function(){
    var div, document = window.document, map, infoWindows, params, divInfo, clickLatLng;
    var kmls = {};

    function isValidId(id) {
        return typeof id === 'number' && id % 1 == 0 && id > 0
    }

    function isValidOptionObject(object) {
        if(typeof object != 'object') return false;
        object.id = parseInt(object.id);
        return isValidId(object.id);
    }

    function removeKml(id) {
        kmls[id].setMap(null);
        delete kmls[id];
    }

    function ltconv(value, not_html, not_deg) {
        var deg = (not_html) ? 'Ð¢Ð' : '&deg;';
        if (not_deg) deg = '';
        if (value == 0)
            return value + deg;

        else if (value > 0)
            return Math.round(value * 100) / 100 + deg + 'N';
        else
            return Math.round(-value * 100) / 100 + deg + 'S';
    }

    function lnconv(value, not_html, not_deg) {
        var deg = (not_html) ? 'Ð¢Ð' : '&deg;';
        if (not_deg) deg = '';
        if (value == 0 || value == 180)
            return value + deg;
        else if (value > 180)
            return Math.round((360 - value) * 100) / 100 + deg + 'W';
        else if (value < 0)
            return Math.round(-value * 100) / 100 + deg + 'W';
        else
            return Math.round(value * 100) / 100 + deg + 'E';
    }

    function addKml(object) {
        if (kmls[object.id]) removeKml(object.id);
        var url = window.location.protocol + '//satbeams.com/!ajax/kml3?action=map&token=overlay&beam=' + object.id, params = [];
        for(var key in object) {
            if(key == 'id' || !object.hasOwnProperty(key)) continue;
            params.push(key + '=' + object[key].toString());
        }

        params.push('rand=' + Math.round(Math.random() * 1000));

        if (params.length) url += '&' + params.join('&');

        var kmlOptions = 
		{
            map: map
            ,url: url
            ,preserveViewport: true
           //,suppressInfoWindows: true
		   ,clickable: false
        };

        var kml = new google.maps.KmlLayer(kmlOptions);
        google.maps.event.addListener(kml, 'click', kmlOnClick);

        kmls[object.id] = kml;
    }

    function SatBeams(_options) {
        if (typeof _options != 'object') throw 'options is not an object';
        if (!(_options.beams instanceof Array)) _options.beams = [];
        for(var i= 0,l=_options.beams.length;i<l;i++){
            if (!isValidOptionObject(_options.beams[i])) delete _options.beams[i];
            addKml(_options.beams[i]);
        }
        params = _options;
        this.setValues(_options);

        var infoWindowsOptions = {

        };
        infoWindows = new google.maps.InfoWindow(infoWindowsOptions);

        var head = document.getElementsByTagName('head')[0];
        var styleElement = document.createElement('link');
        styleElement.type = 'text/css';
        styleElement.rel = 'stylesheet';
        styleElement.href = window.location.protocol + '//satbeams.com/media/system/css/embed.css';
        head.appendChild(styleElement);

        var gcScript = document.createElement('script');
        gcScript.type = 'text/javascript';
        gcScript.src = window.location.protocol + '//satbeams.com/media/system/js/gc3.js';
        gcScript.id = '_script_gc';
        gcScript.charset = 'utf-8';
        head.appendChild(gcScript);
    }
    window['Satbeams'] = SatBeams;
    SatBeams.prototype = new google.maps.OverlayView;

    SatBeams.prototype['draw'] = function() {
    };

    SatBeams.prototype['onRemove'] = function() {
        map = this.getMap();
        div.parentNode.removeChild(div);
        divInfo && divInfo.parentNode.removeChild(divInfo);
        infoWindows.close();

        for (var id in kmls) if (kmls.hasOwnProperty(id)) kmls[id].setMap(map);
    };

    SatBeams.prototype['addBeam'] = function(object) {
        if (!isValidOptionObject(object)) throw 'Error object';
        for(var i= 0,l=this.beams.length;i<=l;i++) {
            if (i == l || object.id == this.beams[i].id) break;
        }
        this.beams[i] = object;
        addKml(object);
    };

    SatBeams.prototype['removeBeam'] = function(id) {
        id = parseInt(id);
        if (!isValidId(id)) throw 'Error id';
        removeKml(id);
    };

    SatBeams.prototype['onAdd'] = function () {
        map = this.getMap();
        if (!params.hideLogo) {
            div = document.createElement('div');
            div.id = 'satbeams_copy';
            div.innerHTML = '<a href="http://satbeams.com" target="_blank"><img border="0" alt="Powered by Satbeams" title="Powered by Satbeams" src="http://satbeams.com/images/logo.png" /></a>';
            map.getDiv().appendChild(div);
        }

        if (params.infowindow || true) {
            divInfo = document.createElement('DIV');
            divInfo.id = 'satbeams_info';
            divInfo.style.opacity = params.opacity || 1;
            divInfo.style.filter = 'alpha(opacity=' + (params.opacity * 100) + ')'
            map.getDiv().appendChild(divInfo);
        }

        for (var id in kmls) if (kmls.hasOwnProperty(id)) kmls[id].setMap(map);
    };

    function ShowInfo(satData) {
        var ll = clickLatLng;
        var gc = new GC(ll.lat(), ll.lng());
        var pos = satData.pos;
        var elev = Math.round(gc.elevation(pos) * 10) / 10.0;
        var dist = Math.round(gc.distance(pos) * 10) / 10.0;
        var tilt = Math.round(gc.tilt(pos) * 10) / 10.0;
        var azz = gc.azimuth(pos);
        var az = Math.round(azz * 10) / 10.0;
        alert(elev);
        var msg = satData.msg;

        var text = '<table cellpadding="0" cellspacing="0" class="satbeams_grid">';
        if (satData.hidenames != 1) {
            if (satData.hidenames != 2) text += '<tr class="satbeams_class_tr"><td id="satbeams_class_td_name" colspan="2">' + satData.name + '</td></tr>';
            if (satData.hidenames != 3) text += '<tr class="satbeams_class_tr"><td id="satbeams_class_td_band" colspan="2" class="satbeams_band_' + satData.band + '">' + satData.fullname + '</td></tr>';
        }
        text += '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["Distance to satellite"] + ':</td><td class="satbeams_class_td_2">' + dist + 'km</td></tr>';
        text += '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["Location"] + ':</td><td class="satbeams_class_td_2">' + ltconv(ll.lat()) + ' ' + lnconv(ll.lng()) + '</td></tr>';

        if (elev <= 0)
            text += '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["Elevation angle"] + ':</td><td class="satbeams_class_td_2"><acronym title="Below the horizon">BTH</acronym></td></tr>'
            + '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["LNB Tilt (skew)"] + ':</td><td class="satbeams_class_td_2"><acronym title="Not applicable">n/a</acronym></td></tr>'
        else
            text += '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["Elevation angle"] + ':</td><td class="satbeams_class_td_2">' + elev + '&deg;</td></tr>'
            + '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["LNB Tilt (skew)"] + ':</td><td class="satbeams_class_td_2">' + tilt + '&deg;</td></tr>';

        text += '<tr class="satbeams_class_tr"><td class="satbeams_class_td_1">' + msg["True azimuth"] + ':</td><td class="satbeams_class_td_2">' + az + '&deg;</td></tr>';
        text += '</table>';
        divInfo.innerHTML = text;
    }

    window['showInfo'] = function(infoWindowData, satData) {
        infoWindows.setContent("<div style='overflow:hidden;'>" + infoWindowData + "</div>");
        infoWindows.open(map);
        if (satData) ShowInfo(satData);
    };

    var loadInfo = function (lat, lng, name) {
        var data = '?lat=' + lat + '&lng=' + lng + '&name=' + name;
        var script = document.createElement('script');
        script.setAttribute('type', 'text/javascript');
        script.setAttribute('src', window.location.protocol + '//satbeams.com/!ajax/poly' + data);
        script.setAttribute('id', 'script_id');
        script.setAttribute('charset', 'utf-8');

        var script_id = document.getElementById('script_id');
        if (script_id) {
            document.getElementsByTagName('head')[0].removeChild(script_id);
        }

        document.getElementsByTagName('head')[0].appendChild(script);
    };

    var kmlOnClick = function(KmlMouseEvent) {
        //infoWindows.setContent("<div style='overflow:hidden;'>Loading...</div>");
        //infoWindows.setPosition(KmlMouseEvent.latLng);
        //infoWindows.open(map);
        //loadInfo(KmlMouseEvent.latLng.lat(), KmlMouseEvent.latLng.lng(), this.getMetadata().name);
        clickLatLng = KmlMouseEvent.latLng;
    };
})();