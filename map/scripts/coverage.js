(function () {
  'use strict';

    var activeButton;
    var activeButton2;
    
  var embed;
  var select;

    var beamsMap = {};
    var beams = [];

    function loadXMLDoc() {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
                if (xmlhttp.status == 200) {
                    var beamsj = xmlhttp.responseText;
                    var beamsExtra = JSON.parse(beamsj);
                    beams = [];
                    for (var i in beamsExtra) {
                        var el = beamsExtra[i];
                        beams.push(el);

                    }
                    select.set([]);
                    select.setData(generateSelectData(beams));
                    select.set(beams.map(function (beam) { return beam.id }));
                }
                else if (xmlhttp.status == 400) {
                    alert('There was an error 400');
                }
                else {
                    alert('something else other than 200 was returned');
                }
            }
        };

        xmlhttp.open("GET", "https://ebq20api.azurewebsites.net/EBusiness/sa.ashx?b=" + activeButton + "\&t=" + activeButton2, true);
        xmlhttp.send();
    }

  function comparer (otherArray) {
    return function (current) {
      return otherArray.filter(function (other) {
        return other.value === current.value;
      }).length === 0;
    };
  }

  function generateSelectData (beams) {
    return beams.map(function (beam) {
      return {
        data: {
          levels: beam.levels,
          mineirp: beam.mineirp,
          bordercolor: beam.bordercolor
        },
        innerHTML: '<span class="beams-option">' + beam.satellite + ' (' + beam.name + ')</span>',
        text: beam.name,
        value: beam.id,
      };
    });
  }

  function toggleButton (event) {
    var id;

    if (event.target.id) {
      id = event.target.id;
    } else {
      id = event.target.parentElement.id;
    }

    if (activeButton) {
      document.getElementById(activeButton).style['background-color'] = '#fff'; //'#414141';
    }

    document.getElementById(id).style['background-color'] = '#FFC500';

    activeButton = id;

    //beams = BEAMS[id].beams;
    loadXMLDoc();

    select.set([]);
    select.setData(generateSelectData(beams));
    select.set(beams.map(function (beam) { return beam.id }));
  }

    function toggleButton2(event) {
        var id;

        if (event.target.id) {
            id = event.target.id;
        } else {
            id = event.target.parentElement.id;
        }

        if (activeButton2) {
            document.getElementById(activeButton2).style['background-color'] = '#fff'; //'#414141';
        }
        document.getElementById(id).style['background-color'] = '#FFC500';
        activeButton2 = id;
        loadXMLDoc();
        select.set([]);
        select.setData(generateSelectData(beams));
        select.set(beams.map(function (beam) { return beam.id }));
    }


  function onChange (embed, selected) {
    var beamsMapKeys = Object.keys(beamsMap);

    if (selected.length > beamsMapKeys.length) {
      selected
        .filter(comparer(embed.beams, true))
        .forEach(function (beam) {
          var levels = beam.data.levels,
              minEIRP = beam.data.mineirp,
              borderColor = beam.data.bordercolor;

          beamsMap[beam.value] = beam;
          embed.addBeam({
            id: beam.value,
            limits: 0,
            minEIRP: minEIRP || 1,
            levels: levels || 1,
            borderColor: borderColor || 'F012BEFF',
            fillColor: 'F012BEFF',
          });
        });
    } else {
      beamsMapKeys
        .map(function (key) { return beamsMap[key]; })
        .filter(comparer(selected, false))
        .forEach(function (beam) {
          delete beamsMap[beam.value];
          embed.removeBeam(beam.value);
        });
    }
  }

  function initMap () {
    var mapOptions = {
      center: new google.maps.LatLng(12.5027604, 19.9678008),
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 3,
    };

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    initQuote(map); 

    embed = new Satbeams({ map: map });

    select = new SlimSelect({
      select: "#beams",
      showSearch: false,
      onChange: onChange.bind(null, embed),
    });

    var buttons = document.querySelectorAll('.mdl-button1');
    for (var i = 0; i < buttons.length; i++) {
      buttons[i].addEventListener('click', toggleButton);
    }

    var buttons2 = document.querySelectorAll('.mdl-button2');
    for (var i = 0; i < buttons2.length; i++) {
          buttons2[i].addEventListener('click', toggleButton2);
    }

    // Pre-select first button
    buttons[0].click();
  }

  document.addEventListener('DOMContentLoaded', initMap);
})();