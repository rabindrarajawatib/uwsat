﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace UWSAT.Services
{
    public class EmailService
    {
        string MailFrom { get; set; }
        string Host { get; set; }
        int Port { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        public EmailService()
        {
            MailFrom = ConfigurationManager.AppSettings["MailFrom"];
            Host = ConfigurationManager.AppSettings["Host"];
            Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            UserName = ConfigurationManager.AppSettings["EmailFromUserName"];
            Password = ConfigurationManager.AppSettings["EmailFromPassword"];

        }
        public bool SendEmail(string mailTo, string Subject, string body)
        {
            try
            {
                MailMessage Msg = new MailMessage();
                Msg.From = new MailAddress(MailFrom);
                Msg.To.Add(mailTo);
                Msg.Subject = Subject;
                Msg.Body = body;
                Msg.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                smtp.Port = Port;
                smtp.Credentials = new System.Net.NetworkCredential(UserName, Password);
                smtp.EnableSsl = true;
                smtp.Send(Msg);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
