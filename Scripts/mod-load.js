﻿var jdata;
var datarates = '';
var latt = '';
var lon = '';
var hlatitute, hlongilute, htech, hapiroot, hurl, hband, calcColSpan, satBeam;
var filSatellite = '', filServices = '', filDataRates = '';

$(document).ready(function () {
    hlatitute = $("#hdnlatitute").val();
    hlongilute = $("#hdnlongitute").val();
    htech = $("#hdntech").val();
    hapiroot = $("#hdnapiroot").val();
    hurl = $("#hdnurl").val();
    hband = $("#hdnband").val();
    convertDMS(hlatitute, hlongilute);
    $.get(hapiroot + "/SLAListHandler.ashx?u=ku&la=" + hlatitute + "&lo=" + hlongilute,
        function (data, status) {
            jdata = JSON.parse(data);
            getSattelite(jdata, "SatelliteName");
            getDistinctItemByMultiProp(jdata, "DRFWD", "DRRTN");
            if (htech == 'NA') {
                BuildUI(jdata, 0);
            }
            else {
                BuildTech(htech);
            }
            if (hurl.indexOf('/contact/') > -1) {
                $("#btncontact").click();
            }
        });

    $.get(hapiroot + "/lo.ashx?la=" + hlatitute + "&lo=" + hlongilute,
        function (data, status) {
            var loc = JSON.parse(data);
            $('#locationdec').text(loc.locationDescription);
        });

});

function BuildUI(jValue, hidetech) {
    try {
        var genHtml = "";
        calcColSpan = (hidetech == 0) ? 7 : 6;
        for (var i = 0; i < jValue.length; i++) {
            if (i == 0) {
                datarates = jValue[i].DRFWD + "/" + jValue[i].DRRTN;
                satBeam = jValue[i].SatelliteName;
            }
            genHtml = genHtml + "<tr class='newselect'>";
            genHtml = genHtml + "<td class='tech-head'><span class='fas fa-plus'></span></td>";
            genHtml = genHtml + "<td>" + jValue[i].SatelliteName + "<span class='hide'>" + jValue[i].sc + "</span>" + "<span class='hide'>" + jValue[i].SlaId + "</span>" + "</td>";
            genHtml = genHtml + "<td>" + jValue[i].SatelliteDescription + "</td>";
            if (hidetech == 0) {
                genHtml = genHtml + "<td class='tech'>" + jValue[i].Technology + "</td>";
            }
            else {
                genHtml = genHtml + "<td class='tech hide'>" + jValue[i].Technology + "</td>";
            }
            genHtml = genHtml + "<td>" + jValue[i].DRFWD + "</td>";
            genHtml = genHtml + "<td>" + jValue[i].DRRTN + "</td>";
            genHtml = genHtml + "<td>" + jValue[i].eirp + "</td>";
            genHtml = genHtml + "<td>" + jValue[i].g_t + "</td>";
            genHtml = genHtml + "</tr>";
            //genHtml = genHtml + "<tr style='display:none;'><td colspan=" + calcColSpan + ">Service Class: " + jValue[i].ServiceClass + "</br>ref_diameter: " + jValue[i].ref_diameter + "<br/>SlaId: " + jValue[i].SlaId + "</td></tr>";
        }
        $('#dtInfo').append(genHtml);
        $('#dtInfo').DataTable();
        $('.dataTables_length').addClass('bs-select');
    } catch (e) {
        console.log(e);
    }
}

$("#satteliteFilter").change(function () {
    filSatellite = $(this).val();
    if (filSatellite != '---Select---')
        filterUIData();
});

$("#dataratefilter").change(function () {
    filDataRates = $(this).val();
    if (filDataRates != '---Select---')
        filterUIData();
});

$('#services').change(function () {
    filServices = $(this).val();
    if (filServices != '---Select---')
        filterUIData();
});

function filterUIData() {

    var filter = jdata;
    if (filSatellite != '') {
        filSatellite = filSatellite.replace(/\s+$/, '');
        filter = filter.filter(function (i, n) {
            return i.SatelliteName.replace(/\s+$/, '') === filSatellite.replace(/\s+$/, '');
        });
    }
    if (filServices != '') {
        filServices = filServices.replace(/\s+$/, '');
        filter = filter.filter(function (i, n) {
            return i.ServiceClass.replace(/\s+$/, '') === filServices;
        });
    }
    if (filDataRates != '') {
        filDataRates = filDataRates.replace(/\s+$/, '');
        var drfwd = filDataRates.substring(0, filDataRates.indexOf("/"));
        var drrtn = filDataRates.substring(filDataRates.indexOf("/") + 1);
        filter = filter.filter(function (i, n) {
            return i.DRFWD.replace(/\s+$/, '') === drfwd &&
                $.trim(i.DRRTN.replace(/\s+$/, '')) === drrtn;
        });
    }
    $('#dtInfo').DataTable().clear().destroy();
    $('#tbInfo').empty();
    $('#dvtech').hide();
    BuildUI(filter, 0);
}

//$('#tbInfo').on("click", ".newselect", function () {
//    var ulink = $(this).find('span')[0].innerText.replace(/\s+$/, '');
//    var slaId = $(this).find('span')[1].innerText.replace(/\s+$/, '');
//    datarates = $(this).find('td')[3].innerText.replace(/\s+$/, '') + "/" +
//        $(this).find('td')[4].innerText.replace(/\s+$/, '');
//    $.get(hapiroot + "/gq.ashx?u=" + ulink + "&s=" + slaId + "",
//        function (data, status) {
//            var sladata = JSON.parse(data);
//            $("#slaInfo").text('');
//            $.each(sladata, function (index, value) {
//                for (var key in value) {
//                    $("#slaInfo").append(key + " : " + value[key] + "<br/>");
//                }
//            });
//        });
//    $('#dvSLA').show();
//});

$('#tbInfo').on("click", ".tech-head", function () {
    var sc = $.trim($(this).closest('td').next('td').find('span')[0].innerText.replace(/\s+$/, ''));
    var slaid = $.trim($(this).closest('td').next('td').find('span')[1].innerText.replace(/\s+$/, ''));
    satBeam = $(this).closest('td').next('td').text();//.innerText.replace(/\s+$/, '');
    satBeam = satBeam.substring(0, satBeam.indexOf(" "))
    $('#trdynamic').remove();
    if ($(this).find('span:nth-child(1)').hasClass('fa-minus')) {
        $(this).find('span:nth-child(1)').removeClass('fa-minus');
        $(this).find('span:nth-child(1)').addClass('fa-plus');
        return;
    }
    $('.tech-head').find('span:nth-child(1)').removeClass('fa-minus');
    $('.tech-head').find('span:nth-child(1)').addClass('fa-plus');
    $(this).find('span:nth-child(1)').removeClass('fa-plus');
    $(this).find('span:nth-child(1)').addClass('fa-minus');
    $('<tr id="trdynamic"><td style="min-height:50px;" colspan=' + (calcColSpan + 1) + '></td></tr>').insertAfter($(this).closest('tr'));
    $.get(hapiroot + "/gq.ashx?sl=" + slaid + "&sc=" + sc,
        function (data, status) {
            data = JSON.parse(data);
            var htmltbl = "<i style='font-size: 24px' class='fa'>&#xf105;</i><br/><table>";
            htmltbl = htmltbl + "<thead><tr><th></th><th>Part</th><th>Description</th><th>Quantity</th><th>Unit Price</th><th>Total</th></tr></thead><tbody>";
            for (var cnt = 0; cnt < data.length; cnt++) {
                htmltbl = htmltbl + "<tr><td>" + (data[cnt].num != null ? data[cnt].num : '') + "</td><td>";
                htmltbl = htmltbl + (data[cnt].Part != null ? data[cnt].Part : '') + "</td><td>";
                htmltbl = htmltbl + (data[cnt].decription != null ? data[cnt].decription : '' || data[cnt].description != null ? data[cnt].description : '') + "</td><td>";
                htmltbl = htmltbl + (data[cnt].Quantity != null ? data[cnt].Quantity : '') + "</td><td>";
                htmltbl = htmltbl + (data[cnt]["Unit Price"] != null ? data[cnt]["Unit Price"] + " $" : '') + "</td><td>";
                htmltbl = htmltbl + (data[cnt].Total != null ? data[cnt].Total + " $" : '') + "</td></tr>";
            }
            htmltbl = htmltbl + "</tbody>";
            htmltbl = htmltbl + "</table>";
            $("#trdynamic").find('td').html(htmltbl);
        });

});

$('#advfilter').click(function () {
    if ($('fieldset').css('display') == 'none') {
        $('fieldset').show();
        $(this).find('span').removeClass('fa-plus');
        $(this).find('span').addClass('fa-minus');
    }
    else {
        $('fieldset').hide();
        $(this).find('span').removeClass('fa-minus');
        $(this).find('span').addClass('fa-plus');
    }
});

$('.custom-sty').click(function () {
    $('.custom-sty').removeClass('btn-success');
    $('.custom-sty-tech').removeClass('btn-default');
    $(this).removeClass('btn-outline-success');
    $(this).addClass('btn-success');
});

$('.custom-sty-tech').click(function () {
    var btntext = $(this).text().replace(/\s+$/, '').toLowerCase();
    location.href = "/mod/" + hband + "/" + btntext + "?la=" + hlatitute + "&lo=" + hlongilute;
});

function BuildTech(techval) {
    $('.custom-sty-tech').each(function (index, value) {
        if (techval.toLowerCase() == $(this).text().toLowerCase()) {
            $(this).removeClass('btn-outline-default');
            $(this).addClass('btn-default');
            $('#dvtech').find('span').text($(this).text());
        }
    });
    $('.tech').addClass('hide');
    //$('.custom-control-input').prop('checked', false);
    var filter = jdata.filter(function (i, n) {
        return i.Technology.replace(/\s+$/, '').toLowerCase() === techval.replace(/\s+$/, '').toLowerCase();
    });
    jdata = filter;
    $('#dtInfo').DataTable().clear().destroy();
    $('#tbInfo').empty();
    BuildUI(filter, 1);
}

$('.custom-control-input').click(function () {
    $('.tech').removeClass('hide');
    $('.custom-sty-tech').removeClass('btn-default');
    $('.custom-sty-tech').addClass('btn-outline-default');
    var btntext = $(this).val();
    var filter = jdata.filter(function (i, n) {
        return i.ServiceClass.replace(/\s+$/, '') === btntext.replace(/\s+$/, '');
    });
    jdata = filter;
    $('#dtInfo').DataTable().clear().destroy();
    $('#tbInfo').empty();
    $('#dvtech').hide();
    BuildUI(filter, 0);
});

$("#btnclear").click(function () {
    filSatellite = '';
    filServices = '';
    filDataRates = '';
    $("#satteliteFilter").val('---Select---');
    $("#services").val('---Select---');
    $("#dataratefilter").val('---Select---');
    if (htech == 'NA') {
        $('#tbInfo').empty();
        BuildUI(jdata, 0);
    }
    else {
        BuildTech(htech);
    }
});

$("#btncontact").click(function () {
    $("#dvContainer").show();
    $("#spmessage").hide();
    $('#btnsave').show();
    $("#splocation").html("<span>Latitude:" + latt + "</span><span style='padding-left:40px;'>Longitute: " + lon + "</span>");
    $("#myModal").modal("show");
    $("#txtDatarates").val(datarates);
});

$('#btnsave').click(function () {
    var noofconuser = $("#txtNumOfUsers").val();
    if (noofconuser == '')
        return;
    var datarates = $("#txtDescription").val();
    var location = $("#splocation").text();
    $("#dvContainer").hide();
    var gc = new GC(hlatitute, hlongilute);
    var pos = 3;
    var elev = Math.round(gc.elevation(pos) * 10) / 10.0;
    if (isNaN(elev))
        elev = ''
    var tilt = -Math.round(gc.tilt(pos) * 10) / 10.0;
    if (isNaN(tilt))
        tilt = '';
    var azz = gc.azimuth(pos);
    if (isNaN(azz))
        azz = '';
    var az = Math.round(azz * 10) / 10.0;
    if (isNaN(az)) az = '';
    var msg = "Terminal location: <span>Latitude:" + latt + "</span><span style='padding-left:40px;'>Longitute: " + lon + "</span><br/>";
    msg = msg + "Satellite beam: " + satBeam + "<br/>";
    if (htech != 'NA')
        msg = msg + "Technology:  " + htech + "<br/>";
    msg = msg + "Frequency band: " + hband + "<br/>";
    msg = msg + "Pointing information: <br/>";
    msg = msg + "<span style='padding-left:20px;'>Elevation: " + elev + "&deg;</span> <br/>";
    msg = msg + "<span style='padding-left:20px;'>Azimuth: -" + tilt + "&deg; ↻</span> <br/>";
    msg = msg + "<span style='padding-left:20px;'>Skew: " + az +"&deg;</span> <br/>";
    $("#spmessage").html(msg);
    $("#spmessage").show();
    $('#btnsave').hide();
    $.post("/sendnotification", { numberOfConUsers: noofconuser, dataRates: datarates, location: location },
        function (data, status) {
            console.log(data);
        });
    //$("#myModal").modal("show");

});

function toDegreesMinutesAndSeconds(coordinate) {
    var absolute = Math.abs(coordinate);
    var degrees = Math.floor(absolute);
    var minutesNotTruncated = (absolute - degrees) * 60;
    var minutes = Math.floor(minutesNotTruncated);
    var seconds = Math.floor((minutesNotTruncated - minutes) * 60);
    return degrees + "&#176 " + minutes + "' " + seconds + "''";
}

function convertDMS(lat, lng) {
    var latitude = toDegreesMinutesAndSeconds(lat);
    var latitudeCardinal = lat >= 0 ? "N" : "S";
    var longitude = toDegreesMinutesAndSeconds(lng);
    var longitudeCardinal = lng >= 0 ? "E" : "W";
    latt = latitude + " " + latitudeCardinal;
    lon = longitude + " " + longitudeCardinal;
    $('#spnlat').html("Latitude: " + latitude + " " + latitudeCardinal);
    $('#spnlon').html("Longitute: " + longitude + " " + longitudeCardinal);
}

function getSattelite(items, propertyName) {
    var mySelect = $('#satteliteFilter');
    var result = [];
    $.each(items, function (index, item) {
        if ($.inArray(item[propertyName], result) == -1) {
            result.push(item[propertyName]);
            mySelect.append(
                $('<option></option>').val(item[propertyName]).html(item[propertyName])
            );
        }
    });
    return result;
}

function getSatteliteWithDesc(items) {
    var result = [];
    var resultDesc = [];
    $.each(items, function (index, item) {
        if ($.inArray(item['SatelliteName'], result) == -1) {
            result.push(item['SatelliteName']);
            resultDesc.push({ name: item['SatelliteName'], description: item['SatelliteDescription'] });
        }
    });
    return resultDesc;
}

function getSatteliteFilData(items, filterValue) {
    var result = [];
    $.each(items, function (index, item) {
        if (item['SatelliteName'] == filterValue) {
            result.push(item);
        }
    });
    return result;
}

function getDistinctItemByMultiProp(items, propertyName1, propertyName2) {
    var result = [];
    var mySelect = $('#dataratefilter');
    $.each(items, function (index, item) {
        if ($.inArray(item[propertyName1].replace(/\s+$/, '') + "/" + $.trim(item[propertyName2]).replace(/\s+$/, ''), result) == -1) {
            var dataratename = item[propertyName1].replace(/\s+$/, '') + "/" + $.trim(item[propertyName2]).replace(/\s+$/, '');
            result.push(dataratename);
            mySelect.append(
                $('<option></option>').val(dataratename).html(dataratename)
            );
        }
    });
    return result;
}

