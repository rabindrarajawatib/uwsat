﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UWSAT
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "mapn",
                url: "mapn",
                defaults: new { controller = "mapn", action = "Index"}
            );

            routes.MapRoute(
                name: "mod",
                url: "mod/{band}",
                defaults: new { controller = "mod", action = "Index", band = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "sendnotification",
                url: "sendnotification",
                defaults: new { controller = "mod", action = "SendNotification" }
            );

            routes.MapRoute(
                name: "modtech",
                url: "mod/{band}/{tech}",
                defaults: new { controller = "mod", action = "Index", band = UrlParameter.Optional, tech=UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "contact",
                url: "contact/{band}",
                defaults: new { controller = "mod", action = "Index", band = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
